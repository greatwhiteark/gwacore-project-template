<?php
/**
 * GWACore entrypoint
 */

// composer autoload
require_once __DIR__.'/../vendor/autoload.php';

// create main config
$config = (
    new Gwa\Core\system\gwConfigLoader(
        __DIR__.'/../app/config',
        getenv('GW_ENVIRONMENT') ?: 'production'
    )
)->load();
$config->setSetting('dir.base', __DIR__ . '/../');

// init system
$system = new Gwa\Core\system\gwSystem($config);

// handle request
$view = $system->setUp()->handleRequest(
    new Gwa\Core\system\gwRequest(
        array_key_exists('REQUEST_METHOD', $_SERVER) ? $_SERVER['REQUEST_METHOD'] : '',
        array_key_exists('gwquery', $_REQUEST) ? $_REQUEST['gwquery'] : '',
        Gwa\Core\system\gwRequest::getPHPEnvironmentVariables()
    )
);

// output
$view->output();

// tear down
$system->tearDown();
