My GWACore Project
==================

## Setup

Create the project skeleton using `composer`:

~~~bash
composer create-project gwa/gwacore-project-template --stability=dev directory/
~~~

* Check settings in `app/config/`.
* Make sure that `app/_cache/` and `app/_logs/` are writable.

## DB Migrations

### Migrate

~~~bash
./vendor/bin/phinx migrate -c app/config/phinx.php
~~~

Windows:

~~~bash
vendor\bin\phinx migrate -c app/config/phinx.php
~~~

### Create a new migration

~~~bash
./vendor/bin/phinx create MigrationDescription -c app/config/phinx.php
~~~