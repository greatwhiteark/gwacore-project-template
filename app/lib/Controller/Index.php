<?php
namespace My\AppNameSpace\Controller;

use Gwa\Core\controller\gwController;

class Index extends gwController
{
    protected function __default()
    {
        $this->createView();
        $this->setTemplate('index.tpl.html');
    }
}
