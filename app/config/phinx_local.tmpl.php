<?php
return [
    'default_migration_table' => 'phinxlog',
    'default_database' => 'env',
    'env' => [
        'adapter' => 'mysql',
        'host'    => '127.0.0.1',
        'name'    => '[db name]',
        'user'    => '[db user]',
        'pass'    => '[db pwd]',
        'port'    => '3306',
        'charset' => 'utf8'
    ]
];
