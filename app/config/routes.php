<?php
/**
 * The Routing config.
 */
return [

    // all request methods
    '*' => [
    ],

    // GET only
    'GET' => [
        ['/', ['Index', 'default']]
    ],

    // POST only
    'POST' => [
    ],

    // CLI only
    'CLI' => [
    ],

    'config' => [
        'namespace' => 'My\\AppNameSpace\\Controller\\'
    ]

];
