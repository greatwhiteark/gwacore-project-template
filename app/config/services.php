<?php

// db
$container['core.db.connection'] = function($container) {
    $db = new \Gwa\Core\database\gwDBConnection($container['config']);
    return $db->connection();
};

// repository manager
$container['core.manager.repositories'] = function($container) {
    return new \Gwa\Core\repository\gwRepositoryManager($container['core.db.connection'], $container['config']);
};

// routes manager
$container['core.manager.routes'] = function($container) {
    $manager = new \Gwa\Core\routing\gwRoutesManager();
    $manager->addRoutesFromFile(
        $container['config']->getPath(\Gwa\Core\system\gwConfig::PATH_CONFIG_DIR) . '/routes.php'
    );
    return $manager;
};

// task factory
$container['factory.task'] = function($container) {
    return new \Gwa\Core\task\gwTaskFactory($container);
};

/* ------- */

// form object factory
$container['factory.formobject'] = function($container) {
    return new \Gwa\Core\forms\gwFormObjectFactory($container['config']);
};

// form factory
$container['factory.form'] = function($container) {
    return new \Gwa\Core\forms\gwFormFactory(
        $container['factory.formobject'],
        $container
    );
};
