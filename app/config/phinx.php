<?php
$env = require(__DIR__ . '/phinx_local.php');
return [
    'paths' => [
        'migrations' => 'app/migrations'
    ],
    'environments' => $env
];
