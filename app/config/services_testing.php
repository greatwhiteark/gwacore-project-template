<?php

// db
$container['core.db.connection'] = function($container) {
    $factory = new \Gwa\Core\test\gwTestDBFactory();
    return $factory->getMigratedConnection(__DIR__ . '/../migrations')->connection();
};
