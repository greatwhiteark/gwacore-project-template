<?php
$config = [];

$config['app.namespace'] = '\My\AppNameSpace';

$config['output.default_generator'] = 'Gwa\Core\output\generator\smarty\gwOutputSmarty';

// Add modules
$config['app.modules'] = [
    // $config['app.namespace'] . '\Module\Backend\Backend'
];

return $config;
