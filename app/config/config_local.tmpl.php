<?php
// database settings
$config['db.host']      = 'localhost';
$config['db.database']  = '';
$config['db.user']      = '';
$config['db.password']  = '';

// baseurl
$config['app.baseurl']  = 'http://';

// show backtrace if error/exception thrown
// on for dev
$config['debug.showbacktrace'] = false;

return $config;
