<?php
$setDefaultAssigns = function($event) {

    $system = $event->getSystem();
    $config = $system->getConfig();

    $system->getController()->assign(
        'app',
        [
            'baseurl'     => $config->getBaseURL(),
            'charset'     => $config->getSetting('charset'),
            'environment' => $config->getEnvironment(),
        ]
    );

};

$system->on('post_request', $setDefaultAssigns);
$system->on('post_httpresponse', $setDefaultAssigns);
